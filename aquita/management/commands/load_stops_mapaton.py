from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point
import argparse
import json

from aquita.models import BusStop


class Command(BaseCommand):
    help = 'Imports stops from a single geojson file, created from the stops \
           collected from the mapaton'

    def add_arguments(self, parser):
        parser.add_argument('filename', type=argparse.FileType('r'))

    def handle(self, *args, **options):
        data = json.loads(options['filename'].read())

        ncreated = 0
        nupdated = 0

        for stop in data['features']:
            _, created = BusStop.objects.update_or_create(
                mapaton_id=stop['properties']['id'], defaults={
                    "point": Point(
                        x=stop['geometry']['coordinates'][0],
                        y=stop['geometry']['coordinates'][1],
                    ),
                    "source": 'mapaton',
                },
            )

            if created:
                ncreated += 1
            else:
                nupdated += 1

            print('.', end='', flush=True)
        print()

        print('{} created, {} updated'.format(ncreated, nupdated))
