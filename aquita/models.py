from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.conf import settings


class BusTag(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class BusRoute(models.Model):
    name = models.CharField(max_length=150)
    desc = models.CharField(
        max_length=500, default=None, null=True, blank=True,
    )
    notes = models.CharField(
        max_length=500, default=None, null=True, blank=True,
    )
    route = models.LineStringField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    verified = models.BooleanField(default=False)
    company = models.CharField(
        max_length=150, null=True, default=None, blank=True,
    )
    color = models.CharField(
        max_length=50, null=True, default=None, blank=True,
    )
    tags = models.ManyToManyField(
        'BusTag', related_name='bus_routes',
        help_text='Letreros que el camión muestra en el frente',
    )

    def __str__(self):
        return self.name


class BusStop(models.Model):
    point = models.PointField(default=Point(**settings.DEFAULT_BUST_STOP))
    name = models.CharField(
        max_length=150, default=None, null=True, blank=True,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    osm_node_id = models.BigIntegerField(
        blank=True, default=None, null=True, unique=True,
        help_text='¿Esta parada está en OSM? ¿Qué ID tiene ese nodo?',
    )
    mapaton_id = models.BigIntegerField(
        blank=True, default=None, null=True, unique=True,
        help_text='Id de parada según como fue extraída de los archivos del '
                  'mapatón',
    )
    source = models.CharField(max_length=20, choices=(
        ('osm', 'OSM'),
        ('mapaton', 'Mapatón'),
        ('hand', 'Creada a mano'),
    ), default='hand')
    verified = models.BooleanField(default=False)

    def __str__(self):
        return str(self.point)
