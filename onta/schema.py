from aquita.models import BusRoute as BusRouteModel
from aquita.models import BusStop as BusStopModel
import graphene
import graphql_geojson


class BusRoute(graphql_geojson.GeoJSONType):
    class Meta:
        model = BusRouteModel
        geojson_field = 'route'


class BusStop(graphql_geojson.GeoJSONType):
    class Meta:
        model = BusStopModel
        geojson_field = 'point'


class Query(graphene.ObjectType):
    bus_routes = graphene.List(BusRoute)
    bus_stops = graphene.List(BusStop)

    def resolve_bus_routes(self, info):
        return BusRouteModel.objects.all()

    def resolve_bus_stops(self, info):
        return BusStopModel.objects.all()


schema = graphene.Schema(query=Query)
